# Goblins

This game is a playground for CoderDojo Berlin related stuff.

Based of the [simple HTML5 canvas game tutorial](http://www.lostdecadegames.com/how-to-make-a-simple-html5-canvas-game/) by [Lost Decade games](lostdecadegames.com).
