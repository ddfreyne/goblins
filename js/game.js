// Create the canvas
var canvas = document.createElement("canvas");
var ctx = canvas.getContext("2d");
canvas.width = 512;
canvas.height = 480;
document.body.appendChild(canvas);



// --- IMAGES ---


var imageLoadCount = 0;

// Background image
var bgImage = new Image();
imageLoadCount++;
bgImage.onload = function () { imageLoadCount--; }
bgImage.src = "img/background.png";

// Hero image
var heroImage = new Image();
imageLoadCount++;
heroImage.onload = function () { imageLoadCount--; }
heroImage.src = "img/hero.png";

// Hero image
var heroWithSwordImage = new Image();
imageLoadCount++;
heroWithSwordImage.onload = function () { imageLoadCount--; }
heroWithSwordImage.src = "img/hero-with-sword.png";

// Monster image
var monsterImage = new Image();
imageLoadCount++;
monsterImage.onload = function () { imageLoadCount--; }
monsterImage.src = "img/monster.png";

// Rock image
var rockImage = new Image();
imageLoadCount++;
rockImage.onload = function () { imageLoadCount--; }
rockImage.src = "img/rock.png";



// --- GAME OBJECTS ---



var hero = {
  x: 0,
  y: 0,
  width: 32,
  height: 32,
  speed: 500, // movement in pixels per second
}

var monsters = [{}, {}, {}, {}, {}];
var monstersCaught = 0;
var numMonsters = 2;

var rock1 = {
  x: 32,
  y: 64,
  width: 100,
  height: 100,
}

var rock2 = {
  x: 150,
  y: 300,
  width: 100,
  height: 100,
}


// --- KEYBOARD CONTROLS ---



var keysDown = {};

addEventListener("keydown", function (e) {
  keysDown[e.keyCode] = true;
}, false);

addEventListener("keyup", function (e) {
  delete keysDown[e.keyCode];
}, false);



// -- RESET ---



var newMonster = function() {
  var monster = {};

  monster.width  = 32;
  monster.height = 32;

  var randomValueForPosition = Math.random();
  var randomValueForChosenSide = Math.random();

  if (randomValueForChosenSide < 0.25) {
    monster.x = randomValueForPosition * canvas.width - monster.width;
    monster.y = canvas.height;
  } else if (randomValueForChosenSide < 0.50) {
    monster.x = randomValueForPosition * canvas.width - monster.width;
    monster.y = - monster.height;
  } else if (randomValueForChosenSide < 0.75) {
    monster.x = - monster.width;
    monster.y = randomValueForPosition * canvas.height - monster.height;
  } else {
    monster.x = 0+canvas.width;
    monster.y = randomValueForPosition * canvas.height - monster.height;
  }

  return monster;
}

var reset = function () {
  hero.x = canvas.width / 2;
  hero.y = canvas.height / 2;

  // Throw the monster somewhere on the screen randomly
  for (var i = 0; i < numMonsters; i++) {
    monsters.push(newMonster());
  }

  // BUG: Massive rush of goblins when we stand still in the middle!
};

var killMonster = function (monster) {
  var index = monsters.indexOf(monster);
  monsters.splice(index, 1);

  // IDEA: When a monster is killed, add two new ones.
}



// --- COLLISION DETECTION ---



var isColliding = function(a, b) {
  return a.x + a.width  > b.x            &&
         a.x            < b.x + b.width  &&
         a.y + a.height > b.y            &&
         a.y            < b.y + b.height;
}

var avoidRock = function (thing, oldX, oldY, rock) {
  if (isColliding(thing, rock)) {
    thing.x = oldX;
    thing.y = oldY;
    return false
  }

  return true
};

var avoidRocks = function (thing, oldX, oldY) {
  if (!avoidRock(thing, oldX, oldY, rock1))
    return false;
  if (!avoidRock(thing, oldX, oldY, rock2))
    return false;
  return true;
};



// -- UPDATING GAME STATE ---



var KEY_SPACE = 32;
var KEY_LEFT  = 37;
var KEY_UP    = 38;
var KEY_RIGHT = 39;
var KEY_DOWN  = 40;

var MAX_SPEED = 0.5;

var monsterSpeed = function () {
  var unboundedSpeed = .1 * (monstersCaught + 1);
  if (unboundedSpeed > MAX_SPEED) {
    return MAX_SPEED;
  } else {
    return unboundedSpeed;
  }
}

var heroHasSword = function() {
  return KEY_SPACE in keysDown;
}

var moveHero = function(modifier) {
  var heroOldX = hero.x;
  var heroOldY = hero.y;

  if (KEY_UP in keysDown) {
    hero.y -= hero.speed * modifier;
    if (hero.y < 0) {
      hero.y=0
    }
  }
  if (KEY_DOWN in keysDown) {
    hero.y += hero.speed * modifier;
    if (hero.y + hero.height > canvas.height) {
      hero.y = canvas.height - hero.height;
    }
  }
  if (KEY_LEFT in keysDown) {
    hero.x -= hero.speed * modifier;
    if (hero.x < 0) {
      hero.x=0
    }
  }
  if (KEY_RIGHT in keysDown) {
    hero.x += hero.speed * modifier;
    if (hero.x + hero.width > canvas.width) {
      hero.x = canvas.width - hero.width;
    }
  }

  avoidRocks(hero, heroOldX, heroOldY);
}

var moveMonster = function(monster, dx, dy) {
  var oldX = monster.x;
  var oldY = monster.y;

  monster.x = monster.x + dx;
  avoidRocks(monster, oldX, oldY);

  var oldX = monster.x;
  var oldY = monster.y;

  monster.y = monster.y + dy;
  avoidRocks(monster, oldX, oldY);
}

var update = function (modifier) {
  moveHero(modifier);

  monsters.forEach(function (monster) {
    var angle = Math.atan2(hero.y - monster.y, hero.x - monster.x);

    var cos = Math.cos(angle);
    var sin = Math.sin(angle);

    var dx = cos * monsterSpeed();
    var dy = sin * monsterSpeed();

    moveMonster(monster, dx, 0);
    moveMonster(monster, 0, dy);

    // Handle clash between hero and monster
    if (isColliding(hero, monster)) {
      if (heroHasSword()) {
        ++monstersCaught;
        killMonster(monster);
      } else {
        reset();
      }
    }
  });
};



// --- DRAWING ---



var render = function () {
  if (imageLoadCount > 0) {
    return;
  }
  ctx.drawImage(bgImage, 0, 0);

  if (heroHasSword()) {
    ctx.drawImage(heroWithSwordImage, hero.x, hero.y);
  } else {
    ctx.drawImage(heroImage, hero.x, hero.y);
  }

  monsters.forEach(function (monster) {
    ctx.drawImage(monsterImage, monster.x, monster.y);
  });

  ctx.drawImage(rockImage, rock1.x, rock1.y, rock1.width, rock1.height);
  ctx.drawImage(rockImage, rock2.x, rock2.y, rock2.width, rock2.height);

  ctx.fillStyle = "rgb(250, 250, 250)";
  ctx.font = "24px Helvetica";
  ctx.textAlign = "left";
  ctx.textBaseline = "top";
  ctx.fillText("Goblins caught: " + monstersCaught, 32, 32);
};



// --- MAIN ---



var main = function () {
  var now = Date.now();
  var delta = now - then;

  update(delta / 1000);
  render();

  then = now;
};

reset();
var then = Date.now();
setInterval(main, 1); // Execute as fast as possible
